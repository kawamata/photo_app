Rails.application.routes.draw do
  get 'posts/new'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'top', to:'users#top', as: :top

  # get 'posts/new', to: 'posts#new', as: new_post

	resources :posts do
		# collection(集合)はidなし、member(個別)はidあり
		member do
			get 'like', to: 'posts#like', as: :like
			post 'comment', to: 'posts#comment', as: :comment
		end
	end


	# resources :users
	get '/', to: 'users#top'

	get '/follower_list/:id', to: 'users#follower_list', as: :follower_list
	get '/follow_list/:id', to:'users#follow_list', as: :follow_list

	get '/:id/follow', to: 'users#follow', as: :follow

	get '/sign_up', to: 'users#sign_up'
	post '/sign_up_process', to: 'users#sign_up_process'

	get '/sign_in', to: 'users#sign_in'
	post '/sign_in_process', to: 'users#sign_in_process'

	get '/sign_out', to: 'users#sign_out', as: :sign_out

	get '/profile/:id', to: 'users#show', as: :profile
	get '/profile/:id/edit', to: 'users#edit', as: :profile_edit
	patch '/profile/:id', to: 'users#update', as: :profile_update
  
end
