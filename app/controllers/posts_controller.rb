class PostsController < ApplicationController
	before_action :authorize
	
	   def index	
	   end	

    def new
    	@post = Post.new
    end

    def create
    	@post = Post.new(post_params)
    	upload_file = params[:post][:upload_file]
    
    	if upload_file.blank?
    		flash[:danger] = '画像アップロード必須'
    		redirect_to new_post_url
    	else
    		# 画像のファイル名取得
    		upload_file_name = upload_file.original_filename
    		# public/images のフォルダパスを作成（ここに画像を格納）
    		# Rails.root を使うと、プロジェクトのルートパスを取得できる
    		output_dir = Rails.root.join('public', 'images')
    		# フォルダのパスと画像のファイル名を連結して画像のファイルパスを作成
    		output_path = output_dir + upload_file_name
    		# 画像をアップロード（バイナリで書き込めるモード ('w+b') ）
    		# 画像データを読み込み、writeメソッドで実際にファイルにデータを書き込む
  			File.open(output_path, 'w+b') do |f|
  			  f.write(upload_file.read)
			  end
			# post_images はpostsに紐づいてるので、以下のように保存可能
    		@post.post_images.new(name: upload_file_name)
    		if @post.save
    			flash[:success] = "投稿完了"
    			redirect_to top_url
    		else
    			flash[:danger] = '投稿失敗'
    			render 'new'
    		end
    	end
    end

    def destroy
    	@post = Post.find(params[:id])

    	if @post.destroy
	    	flash[:success] = '削除完了'
	    	redirect_to top_url
    	else
    		flash[:danger] = '削除失敗'
    		redirect_to top_url
    	end
    end

	# 認証チェック
  def authorize
	redirect_to users_sign_in_url unless user_signed_in?
  end

  def like
  	@post = Post.find(params[:id])
  	# データが存在するか確認する時はfindメソッドではなくexistsメソッドを使うと便利（存在確認のみ）
  	if Postlike.exists?(post_id: @post.id, user_id: current_user.id)
  		Postlike.find_by(post_id: @post.id, user_id: current_user.id).destroy
  		redirect_to top_url
  	else
  		Postlike.create(post_id: @post.id, user_id: current_user.id)
  		redirect_to top_url
  	end
  end

  def comment
    @post = Post.find(params[:id])
  	@post.post_comments.create(post_comment_params)
  	redirect_to top_url
  end

    private
    	def post_params
    		params.require(:post).permit(:caption).merge(user_id: current_user.id)
    	end

    	# ここにpost.id が無いのは、紐付けしてるから自動的に格納されるため
      # mergeを使うことで、予めuser_idに入れるデータをここで格納しておけるので便利
    	def post_comment_params
    		params.require(:post_comment).permit(:comment).merge(user_id: current_user.id)
    	end
end
