class UsersController < ApplicationController
	before_action :authorize, except: [:sign_up, :sign_up_process, :sign_in, :sign_in_process]
	before_action :redirect_to_top_if_signed_in, only: [:sign_up, :sign_in]

  def top
    if params[:word].present?
      # キーワード検索処理
      # SELECT * FROM posts WHERE caption LIKE '%キーワード%' ORDER BY id DESC;
      @posts = Post.where("caption like ?", "%#{params[:word]}%").order("id desc").page(params[:page])
      flash[:notice] = '検索結果'
    else
      @posts = Post.all.order("id desc").page(params[:page])
    end
    # レコメンド機能
    # pluckを使うと、そのカラムに該当する値を全て配列で取得可能
    @recommends = User.where.not(id: current_user).where.not(id: current_user.follows.pluck(:follow_user_id)).limit(3)
  end

  def show
  	@user = User.find(params[:id])
    @posts = @user.posts
  end

  def edit
    @user = User.find(current_user.id)
  end

  def update
    # アップデート時に初めてプロフィール画像登録が可能
    upload_file = params[:user][:image]
    if upload_file.present?
      # プロフィール更新時にimageが送られていたら、以下の処理へ
      # 画像のファイル名取得（original_filename はファイル名取得のメソッド）
      upload_file_name = upload_file.original_filename
      # public/images のフォルダパスを作成
      # Rails.root を使うと、プロジェクトのルートパスを取得できる
      output_dir = Rails.root.join('public', 'users')
      # フォルダのパスと画像のファイル名を連結して画像のファイルパスを作成
      output_path = output_dir + upload_file_name
      # 画像をアップロード（バイナリで書き込めるモード ('w+b') ）
      # 画像データを読み込み、writeメソッドで実際にファイルにデータを書き込む
      File.open(output_path, 'w+b') do |f|
        f.write(upload_file.read)
      end
      # mergeメソッドで画像データを追加してアップロード
      current_user.update(user_params.merge({image: upload_file_name}))
      flash[:success] = '更新完了'
      redirect_to profile_url
    else
      current_user.update(user_params)
      flash[:success] = '更新完了'
      redirect_to profile_url
    end
  end

# （このユーザーをフォローしてる人）
  def follower_list
  	@user = User.find(params[:id])
    @followers = User.where(id: Follow.where(follow_user_id: @user.id).pluck(:user_id))
  end

# （このユーザーがフォローしてる人）
  def follow_list
  	@user = User.find(params[:id])
    # pluckメソッド は、指定した列を配列として取得する
    @users = User.where(id: Follow.where(user_id: @user.id).pluck(:follow_user_id))
  end

  def sign_up
  	@user = User.new
  	render layout: "application_not_login"
  end

  def sign_up_process
  	user = User.new(user_params)
  	if user.save
  		flash[:success] = "ユーザー登録完了"
  		user_sign_in(user)
  		redirect_to profile_url(user)
  	else
  		flash[:danger] = "ユーザー登録失敗"
  		redirect_to sign_up_url
  	end
  end

  def sign_in
  	@user = User.new
  	render layout: "application_not_login"
  end

  def sign_in_process
  	# パスワードをmd5に変換（passwordは、sign up時に、md5に変換されて保存しているため
  	password_md5 = User.generate_password(user_params[:password])
    # user変数には trueかfalseが
  	user = User.find_by(email: user_params[:email], password: password_md5)
  	if user
      # サインインに成功したら、sessionを作成
  		user_sign_in(user)
  		redirect_to top_path
  	else
  		flash[:danger] = 'サインイン失敗'
  		redirect_to sign_in_url
  	end
  end

  def sign_out
  	user_sign_out
  	flash[:success] = 'サインアウトしました'
  	redirect_to sign_in_path
  end

	# 認証チェック
  def authorize
	redirect_to sign_in_url unless user_signed_in?
  end

  # フォローするか否かの処理
  def follow
    @user = User.find(params[:id])
    if Follow.exists?(user_id: current_user.id, follow_user_id: @user.id)
      Follow.find_by(user_id: current_user.id, follow_user_id: @user.id).destroy
      redirect_back(fallback_location: top_url, notice: "フォローを更新しました。")
    else
      Follow.create(user_id: current_user.id, follow_user_id: @user.id)
      # 直前のページにリダイレクトする場合は、redirect_backメソッド使用
      # もし直前のページがない場合は、fallback_locationオプションで指定されたページへ遷移
      redirect_back(fallback_location: top_url, notice: "フォローを更新しました。")
    end
  end


  private 
  	def user_params
  		params.require(:user).permit(:name, :email, :comment, :password)
  	end
end
