class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # ↓によって、全てのcontrollerでuser helperが使える
  include UsersHelper
end
