class User < ApplicationRecord
	has_many :posts
	has_many :postlikes
	has_many :post_comments
	has_many :follows
	# follow_user_idをUserのuser.idに紐付けるため、明示する（user_idは自動紐付けのため不要）
	has_many :followers, foreign_key: :follow_user_id, class_name: "Follow"


	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :name, presence: true
	validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}, uniqueness: true
	validates :password, presence: true, length:{minimum: 6}


	# データの保存前（sign_up_processのUser）に、パスワードを暗号化するメソッド(convert_password)を実行するよう設定
	# before_save でなく before_create にする事で、update時はpassword更新されない
	before_create :convert_password

	# パスワードを暗号化するメソッド
	# ここのselfは、生成されたインスタンスに代わるもの=> sign_up_processのUser.newインスタンス
	def convert_password
		self.password = User.generate_password(self.password)
	end

	# selfつきメソッドを特異メソッド（クラスメソッド）と呼ぶ
	# Userモデルに対して、直接呼び出せる（上記例convert_password）
	# パスワードをmd5に変換するメソッド
	def self.generate_password(password)
		salt = "h!hgamcRAdh38bajhvgai17ysvb"
		Digest::MD5.hexdigest(salt + password)
	end

	# ユーザーがフォローされているかどうかを判定
	def followed_by?(user)
		user.follows.exists?(follow_user_id: self.id)
	end
end
