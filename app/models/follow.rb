class Follow < ApplicationRecord
  belongs_to :user

  belongs_to :follow_user, class_name: 'User'
end

# フォロー機能を作るために ⬇️
# フォローする側のuser.id
# フォローされる側のuser.id と２つのuser.idがforeign-keyをして必要となる
# このため、follow_user_id（もうひとつのuser.id）は別の名前を使用しているため
# 明示的にUserのidと紐づける必要がある


# 外部キーとは、他のテーブルのキー（カラム）のこと
# followモデルでは、user_id と follow_user_id がUserモデルのid の外部キーである
# user_idは、この名前の通り、user.idに自動的に紐づくが、follow_user_id のように Userモデルのuser.idと紐付けたいけど異なる名前の時は、明示が必要

# ここで、class_name が登場
# これは、関連するモデル名を指定する。関連名と参照先のモデル名を分けたい場合に使う
# （つまり、follow_user_idの場合）
# ⬆︎つまり、メインのuser_idの他に、user_id（フォローする相手の）が必要になるがカラム名がダブらない様にする仕様

# belongs_to :follow_user （これはつまり、follow_user.id のこと）としてるが、FollowUserモデルは実際はないため、Userモデルを参照する事を明示（user.idとの紐付け）

# 反対に、Userモデルでも、
# 	has_many :followers, foreign_key: :follow_user_id, class_name: "Follow"
# とし、follow_user_id（外部キー）に Userモデルが紐づいてることを明示する


# 参考：https://qiita.com/takeoverjp/items/bb56d6a8eae191cd3732
# 参考：https://morizyun.github.io/ruby/active-record-belongs-to-has-one-has-many.html