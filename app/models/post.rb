class Post < ApplicationRecord
  paginates_per 3
  
  belongs_to :user
  has_many :post_images, dependent: :destroy
  has_many :postlikes, dependent: :destroy
  has_many :post_comments, dependent: :destroy

  def like_from?(user)
  	self.postlikes.exists?(user_id: user.id)
  end
end
